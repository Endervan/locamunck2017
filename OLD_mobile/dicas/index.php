
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- DICAS titulo -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top120 bottom15">
			<div class="col-xs-12 text-center">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_dicas.png" alt="">
				<h4>CONFIRA NOSSAS DICAS</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/seta_geral.png" alt="">
			</div>
		</div>
	</div>
</div>
</div>
<!-- ======================================================================= -->
<!-- DICAS titulo -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- nossas dicas  -->
<!-- ======================================================================= -->
<div class="container top95 bottom50">
  <div class="row">
    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">ÚLTIMAS DICAS</h1>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND()");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-12 padding0 dicas_geral">
          <div class="col-xs-6 top30">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200 ,141 , array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            </div>
            <div class="col-xs-6 top30">
              <div class="dica_desc"><h1><span><?php Util::imprime($row[descricao],200); ?></span></h1></div>
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas dicas  -->
  <!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
