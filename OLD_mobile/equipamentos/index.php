
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- EQUIPAMENTOS titulo -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top120 bottom60">
			<div class="col-xs-12 text-center equip_desc">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_equipamentos.png" alt="">
				<h4>CONHEÇA NOSSOS EQUIPAMENTOS</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/seta_geral.png" alt="">
			</div>
		</div>
	</div>
</div>
</div>
<!-- ======================================================================= -->
<!-- EQUIPAMENTOS titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- Equipamentos decricao  -->
<!-- ======================================================================= -->
<div class="container top145 bottom50">
	<div class="row bg_branco">
		<div class="col-xs-12">
			<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>

			<div class="top30 equip_descricao">
				<p><?php Util::imprime($row[descricao]); ?></p>
			</div>
		</div>

		<?php
		$result = $obj_site->select("tb_equipamentos","ORDER BY RAND()");
		if(mysql_num_rows($result) > 0){
			while($row = mysql_fetch_array($result)){
				?>
				<div class="col-xs-6 equip_geral top15">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>">
						<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 350, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
					</a>
					<div class="top10 equip_titulo">
						<h2><?php Util::imprime($row[titulo],500); ?></h2>
					</div>

					<div class="top5">
						<p><span>MODELO : <?php Util::imprime($row[modelo]); ?></span></p>
					</div>

				</div>

				<?php
			}
		}
		?>

	</div>
</div>

<!-- ======================================================================= -->
<!--  Equipamentos decricao  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
