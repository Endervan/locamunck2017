<div class="topo-geral">

  <div class="container">
    <div class="row">
      <div class="col-xs-12 pt15 topo">

        <?php if (!empty($config[telefone2])): ?>
          <div class="col-xs-6 padding0">
            <div class="media-left media-middle">
              <h2 class="media-heading">
                <span><i class='fa fa-whatsapp right5'></i><?php Util::imprime($config[ddd2]) ?></span>
                <?php Util::imprime($config[telefone2]) ?>
              </h2>
            </div>
            <div class="media-body">
              <a href="tel:+55<?php Util::imprime($config[ddd2]) ?> <?php Util::imprime($config[telefone2]) ?>" class="btn btn_transparente_topo">CHAMAR</a>
            </div>

          </div>
        <?php endif; ?>


        <div class="col-xs-6">
          <div class="media-left media-middle">
            <h2 class="media-heading">
              <span><i class='fa fa-phone right5'></i><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h2>
          </div>
          <div class="media-body">
            <a href="tel:+55<?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?>" class="btn btn_transparente_topo">CHAMAR</a>
          </div>
        </div>

      </div>

    </div>
  </div>



  <div class="container">
    <div class="row bg-topo">
      <div class="col-xs-4 logo text-center">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile">
          <img class="top5" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
        </a>
      </div>

      <div class="col-xs-4">

        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-menu">
          <img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_menu.jpg" alt="">
        </a>

      </div>

      <div class="col-xs-4">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-topo-itens-orcamento">
          <img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_itens_orcamento.jpg" alt="">
        </a>

      </div>

    </div>
  </div>
</div>
</div>
