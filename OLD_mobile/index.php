<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                <div class="carousel-caption text-center">
                  <?php /*<h1><?php Util::imprime($imagem[titulo]); ?></h1>
                  <p><?php Util::imprime($imagem[legenda]); ?></p>*/ ?>
                  <?php if (!empty($imagem[url])) { ?>
                    <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                    <?php } ?>
                  </div>
                </div>

                <?php
                $i++;
              }
            }
            ?>


          </div>



          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>



        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- slider	-->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!-- SERVICOS HOME -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row ">

        <div class="col-xs-7 empresa_titulo servico_subir">
          <h1 class="pb15">NOSSOS SERVIÇOS</h1>
        </div>

        <div class="col-xs-12">
          <?php
          $result = $obj_site->select("tb_tipos_servicos", "order by rand() limit 4");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              //  busca a qtd de produtos cadastrados
              // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


              switch ($i) {
                case 1:
                $cor_class = 'DIREITO_TRIBUTÁRIO';
                break;
                case 2:
                $cor_class = 'DIREITO_CIVIL';
                break;
                case 3:
                $cor_class = 'DIREIT_ DO_TRABALHO';
                break;
                case 4:
                $cor_class = 'DIREITO_TRIBUTÁRIO';
                break;
                case 5:
                $cor_class = 'DIREITO_CIVIL';
                break;

                default:
                $cor_class = 'DIREITO_DO_TRABALHO';
                break;
              }
              $i++;
              ?>
              <!-- ======================================================================= -->
              <!--ITEM 01 REPETI ATE 6 ITENS   -->
              <!-- ======================================================================= -->
              <div class="col-xs-12 servicos pt15 pb15 <?php echo $cor_class; ?> ">

                <div class="bordar_servicos pb10">
                  <div class="media">
                    <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                    </a>
                    <div class="media-body">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                        <h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
                        <div class=" servicos_desc">
                          <p><?php Util::imprime($row[descricao]); ?></p>
                        </div>
                      </a>
                    </div>

                  </div>
                </div>


              </div>

              <?php
            }
          }
          ?>
        </div>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- EQUIPAMANTOS HOME -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <div class="col-xs-12 empresa_titulo">
        <h1 class="pb20">NOSSOS EQUIPAMENTOS</h1>
      </div>

      <?php
      $result = $obj_site->select("tb_equipamentos","ORDER BY RAND() limit 2");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-6 equip_geral top15">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 350, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="top10 equip_titulo">
              <h2><?php Util::imprime($row[titulo],200); ?></h2>
            </div>

            <div class="top5">
              <p><span>MODELO : <?php Util::imprime($row[modelo]); ?></span></p>
            </div>

          </div>



          <?php
        }
      }
      ?>

    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- EQUIPAMANTOS HOME -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->
<div class="container ">
  <div class="row top45">

    <div class="col-xs-12 empresa_home">

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
      <h6><span>CONHEÇA A </span>LOKAMUNCK</h6>
      <img class="top15" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg_empresa_home.jpg" alt="">
      <div class="top15 empresa_desc">
        <p><?php Util::imprime($row[descricao]); ?></span></p>
      </div>

      <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" class="btn btn_empresa_home top20">
        SAIBA MAIS
      </a>

      <a href="<?php Util::imprime($config[link_place]); ?>" class="btn btn_empresa_home top20 left15" target="_blank">
        SAIBA COMO CHEGAR
      </a>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- nossas dicas  -->
<!-- ======================================================================= -->
<div class="container top50 bottom50">
  <div class="row">
    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">NOSSAS DICAS</h1>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 2");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-12 padding0 dicas_geral">
          <div class="col-xs-6 top30">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200 ,141 , array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            </div>
            <div class="col-xs-6 top30">
              <div class="dica_desc"><h1><span><?php Util::imprime($row[descricao],200); ?></span></h1></div>
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas dicas  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/modal.php') ?>
  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->

</body>

</html>





<?php require_once('./includes/js_css.php') ?>
