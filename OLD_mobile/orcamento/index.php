
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
	//  SELECIONO O TIPO
	switch($_GET[tipo])
	{
		case "produto":
		$id = $_GET[id];
		unset($_SESSION[solicitacoes_produtos][$id]);
		sort($_SESSION[solicitacoes_produtos]);
		break;
		case "servico":
		$id = $_GET[id];
		unset($_SESSION[solicitacoes_servicos][$id]);
		sort($_SESSION[solicitacoes_servicos]);
		break;
		case "piscina_vinil":
		$id = $_GET[id];
		unset($_SESSION[piscina_vinil][$id]);
		sort($_SESSION[piscina_vinil]);
		break;
	}

}


?>

<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 19); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- TITULO EMPRESA  -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top50 bottom40">
			<div class="col-xs-12 text-center titulo_trabalhe">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_orcamento.png" alt="">
				<h4>ENVIE SEU ORÇAMENTO</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
			</div>
		</div>

	</div>
</div>
<!-- ======================================================================= -->
<!-- TITULO EMPRESA  -->
<!-- ======================================================================= -->


<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
	<div class="container">
		<div class="row bottom40">

			<!-- ======================================================================= -->
			<!-- CARRINHO  -->
			<!-- ======================================================================= -->
			<div class="col-xs-12  tabela_carrinho pt25">
				<div class="bg_cinza pg10">
					<h5 class="top20"><span >ITENS SELECIONADOS</span></h5>
					<table class="table table-condensed col-xs-12">
						<tbody>

							<?php
							if(count($_SESSION[solicitacoes_produtos]) > 0){
								for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
									$row = $obj_site->select_unico("tb_equipamentos", "idequipamento", $_SESSION[solicitacoes_produtos][$i]);
									?>
									<tr>
										<td>
											<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?>
										</td>
										<td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
										<td class="text-center">
											QTD.<br>
											<input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
											<input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
										</td>
										<td class="text-center">
											<a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
												<i class="fa fa-times-circle fa-2x top20"></i>
											</a>
										</td>
									</tr>
									<?php
								}
							}
							?>



							<?php
							if(count($_SESSION[solicitacoes_servicos]) > 0){
								for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
									$row = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_SESSION[solicitacoes_servicos][$i]);
									?>
									<tr>
										<td>
											<img class="carrinho_servcos_orcamento" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />

											<!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
										</td>
										<td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
										<td class="text-center">
											QTD.<br>
											<input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
											<input name="idtiposervico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
										</td>
										<td class="text-center">
											<a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
												<i class="fa fa-times-circle fa-2x top20"></i>
											</a>
										</td>
									</tr>
									<?php
								}
							}
							?>


						</tbody>
					</table>
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- CARRINHO  -->
			<!-- ======================================================================= -->

			<div class="col-xs-12">

				<div class="fundo_formulario">
					<h5 class="top20 left15"><span>CONFIRME SEUS DADOS</span></h5>
					<!-- formulario orcamento -->
					<div class="top20">
						<div class="col-xs-6">
							<div class="form-group input100 has-feedback">
								<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group  input100 has-feedback">
								<input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">

							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="top20">
						<div class="col-xs-6">
							<div class="form-group  input100 has-feedback">
								<input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">

							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group  input100 has-feedback">
								<input type="text" name="localidade" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">

							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="top15">
						<div class="col-xs-12">
							<div class="form-group input100 has-feedback">
								<textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="OBSERVAÇÕES"></textarea>

							</div>
						</div>
					</div>

					<!-- formulario orcamento -->
					<div class="col-xs-12 text-right">
						<div class="top15 bottom25">
							<button type="submit" class="btn btn_formulario" name="btn_contato">
								ENVIAR
							</button>
						</div>
					</div>

				</div>
				<!--  ==============================================================  -->
				<!-- FORMULARIO-->
				<!--  ==============================================================  -->
			</div>

		</div>
	</div>
</div>

</form>






<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>



<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->




<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

	//  CADASTRO OS PRODUTOS SOLICITADOS
	for($i=0; $i < count($_POST[qtd]); $i++){
		$dados = $obj_site->select_unico("tb_equipamentos", "idequipamento", $_POST[idproduto][$i]);

		$produtos .= "
		<tr>
		<td><p>". $_POST[qtd][$i] ."</p></td>
		<td><p>". utf8_encode($dados[titulo]) ."</p></td>
		</tr>
		";
	}

	//  CADASTRO OS SERVICOS SOLICITADOS
	for($i=0; $i < count($_POST[qtd_servico]); $i++){
		$dados = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_POST[idtiposervico][$i]);

		$produtos .= "
		<tr>
		<td><p>". $_POST[qtd_servico][$i] ."</p></td>
		<td><p>". utf8_encode($dados[titulo]) ."</p></td>
		</tr>
		";
	}




	//  ENVIANDO A MENSAGEM PARA O CLIENTE
	$texto_mensagem = "
	O seguinte cliente fez uma solicitação pelo site. <br />

	Nome: $_POST[nome] <br />
	Email: $_POST[email] <br />
	Telefone: $_POST[telefone] <br />
	Localidade: $_POST[localidade] <br />
	Mensagem: <br />
	". nl2br($_POST[mensagem]) ." <br />

	<br />
	<h2> Produtos selecionados:</h2> <br />

	<table width='100%' border='0' cellpadding='5' cellspacing='5'>
	<tr>
	<td><h4>QTD</h4></td>
	<td><h4>PRODUTO</h4></td>
	</tr>
	$produtos
	</table>

	";



	Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento do site"), $texto_mensagem, $nome_remetente, $email);
	Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
	unset($_SESSION[solicitacoes_produtos]);
	unset($_SESSION[solicitacoes_servicos]);
	unset($_SESSION[piscinas_vinil]);
	Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>





<script>
$(document).ready(function() {
	$('.FormContatos').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'fa fa-check',
			invalid: 'fa fa-remove',
			validating: 'fa fa-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {
						message: 'Por favor insira seu nome'

					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'insira seu email'
					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {
						message: 'Informe um telefone'
					},
					phone: {
						country: 'BR',
						message: 'Informe um telefone válido.'
					}
				},
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			localidade1: {
				validators: {
					notEmpty: {

					}
				}
			},
			mensagem1: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>
