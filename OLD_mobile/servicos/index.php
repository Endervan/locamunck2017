
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- SERVICOS titulo -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top80 bottom40">
			<div class="col-xs-12 text-center equip_desc">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_servicos.png" alt="">
				<h4>CONHEÇA NOSSOS SERVIÇOS</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/seta_geral.png" alt="">
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- SERVICOS titulo -->
	<!-- ======================================================================= -->

	<!-- ======================================================================= -->
	<!-- SERVICOS HOME -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row ">

			<div class="col-xs-12">
				<?php
				$result = $obj_site->select("tb_tipos_servicos", "order by rand() limit 4");
				if (mysql_num_rows($result) > 0) {
					while($row = mysql_fetch_array($result)){
						//  busca a qtd de produtos cadastrados
						// $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


						switch ($i) {
							case 1:
							$cor_class = 'DIREITO_TRIBUTÁRIO';
							break;
							case 2:
							$cor_class = 'DIREITO_CIVIL';
							break;
							case 3:
							$cor_class = 'DIREIT_ DO_TRABALHO';
							break;
							case 4:
							$cor_class = 'DIREITO_TRIBUTÁRIO';
							break;
							case 5:
							$cor_class = 'DIREITO_CIVIL';
							break;

							default:
							$cor_class = 'DIREITO_DO_TRABALHO';
							break;
						}
						$i++;
						?>
						<!-- ======================================================================= -->
						<!--ITEM 01 REPETI ATE 6 ITENS   -->
						<!-- ======================================================================= -->
						<div class="col-xs-12 servicos  pt15 pb15 <?php echo $cor_class; ?> ">

							<div class="col-xs-12 bg_branco">
								<div class="media pt20 bottom15">
									<a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
									</a>
									<div class="media-body">
										<a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

											<h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
											<div class=" servicos_desc">
												<p><?php Util::imprime($row[descricao]); ?></p>
											</div>
										</a>
									</div>

								</div>
							</div>


						</div>

						<?php
					}
				}
				?>
			</div>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- SERVICOS HOME -->
	<!-- ======================================================================= -->

	<div class="clearfix"></div>
	<div class="top220"></div>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
