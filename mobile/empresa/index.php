<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  
  .bg-internaOLD{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_empresa]); ?>) top 200px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row clearfix ">
      <div class="">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg-empresa.jpg"
          width="380"
          height="160"
          layout="responsive"
          alt="AMP"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row">
    <div class="col-12 empresa_geral top20">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
    </div>
    <div class="col-12 ">
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>


  <div class="row">
    <div class="col-12 top20">
      <div class="lista-unidades">
          <div class="col-12 text-center titulo-unidade">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
              <p class="text-center"><?php Util::imprime($row[titulo]); ?></p>
          </div>
          <div class="col-12 top20">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>    
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-12 top20">
      <div class="lista-unidades">
          <div class="col-12 text-center titulo-unidade">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
              <p class="text-center"><?php Util::imprime($row[titulo]); ?></p>
          </div>
          <div class="col-12 top20">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>    
      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-12 top20">
      <div class="lista-unidades">
          <div class="col-12 text-center titulo-unidade">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
              <p class="text-center"><?php Util::imprime($row[titulo]); ?></p>
          </div>
          <div class="col-12 top20">
            <p><?php Util::imprime($row[descricao]); ?></p>

            <?php
            $result = $obj_site->select("tb_atuacoes","LIMIT 15");
            if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
                ?>
                <div class="col-12 padding0  top10 lato_black">
                  <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
                </div>
                <?php
              }
            }
            ?>

          </div>    
      </div>
    </div>
  </div>





  

  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn-verde"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
    </a>
  </div>


</div>

  <div class="row empresa_geral top35">
      <!-- <div class="col-12   text-center">
          <h4>NOSSA LOCALIZAÇÃO</h4>
          <amp-img class="top10" src="<?php //echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                   height="24" width="217"></amp-img>

      </div> -->

      

  </div>




  <?php require_once("../includes/unidades.php"); ?>




<?php //require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>



</html>
