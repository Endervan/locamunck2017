<?php
//$domain_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
//header("AMP-Access-Control-Allow-Source-Origin: " . $domain_url);
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_carrinho = new Carrinho();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];





?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  <?php require_once("../includes/css_personalizado.php");  //  CSS PERSONALIZADO ?>
  <?php require_once("../includes/css_personalizado.php");  //  CSS PERSONALIZADO ?>

  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

  
</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  padding0 localizacao-pagina interna text-center">
          <h5><?php Util::imprime($banner[legenda_1]); ?></h5>
          <?php if (isset($banner[legenda_2])) : ?>
              <h6 ><?php Util::imprime($banner[legenda_2]); ?></h6 class="open">
          <?php endif; ?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->
  <?php if ($_GET[enviado] == 'sim'): ?>
      <div class="col-12  text-center top40">
          <h4>Compra efetuada com sucesso.</h4>
          <a class="btn btn_2 top15" href="<?php echo Util::caminho_projeto() ?>/mobile/contato">
              VER MAIS PRODUTOS
          </a>
      </div>
  <?php else: ?>
    <?php require_once("../includes/carrinho_passo_3.php") ?>
  <?php endif; ?>
  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->



  <div class="row ">
    <div class="col-12 top25">
      <a class="btn col-12 padding0 btn_1"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
    </a>
    </div>


  </div>

  </div>



  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->




  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
