<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- DICAS titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom70">
      <div class="col-xs-12">
        <h5>SEMPRE QUE PRECISAR
          <span class="clearfix">FALE CONOSCO</span>
        </h5>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- DICAS titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--FALE CONOSCO-->
<!-- ======================================================================= -->
<div class='container'>
  <div class="row top10">

    <div class="col-xs-8 dicas_Dentro padding0">
      <div class="sombra_fale"></div>

      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
      <div class="col-xs-12 top20">
        <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="fundo_formulario">
            <!-- formulario orcamento -->
            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                  <span class="fa fa-user form-control-feedback top15"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                  <span class="fa fa-envelope form-control-feedback top15"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>


            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                  <span class="fa fa-phone form-control-feedback top15"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                  <span class="fa fa-star form-control-feedback"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top15">
              <div class="col-xs-12">
                <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="25" rows="14" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span>
                </div>
              </div>
            </div>

            <div class="col-xs-6 top30">

            </div>
            <div class="col-xs-6 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn_formulario1" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>
        </form>
      </div>
      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->


    </div>


    <div class="col-xs-4 contato_dados top195">

      <h1 class="left60">
        <span><i class="fa fa-phone right10"></i><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span>
      </h1>
      <?php if (!empty($config[telefone2])): ?>
        <h1 class="top10 left60">
          <span><i class="fa fa-whatsapp right10"></i><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span>
        </h1>
      <?php endif; ?>

      <div class="col-xs-10 col-xs-offset-2 contatos-btn-opcoes padding0 top10">

        <a href="javascript:void(0);" class="active">
          <span class="left20">FALE CONOSCO</span>
        </a>

        <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" class="">

          <span class="left20">TRABALHE CONOSCO</span>
        </a>



      </div>

      <div class="imagem pull-right top20">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba_como_chegar.png" alt="">
      </div>
    </div>



  </div>
</div>
<!-- ======================================================================= -->
<!--FALE CONOSCO-->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class='container-fluid'>
  <div class="row mapa">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="592" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->





<div class="top105"></div>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  $texto_mensagem = "
  Nome: ".($_POST[nome])." <br />
  Localidade: ".($_POST[localidade])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Email: ".($_POST[email])." <br />
  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";


  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
