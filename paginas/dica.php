<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_dicas.png" alt="">
        <h4>CONFIRA NOSSAS DICAS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- DICAS titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--DICAS DENTRO-->
<!-- ======================================================================= -->
<div class='container'>
  <div class="row top170">

    <div class="col-xs-12 dicas_Dentro">

      <div class="top30">
        <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
      </div>
      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",775, 344, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>
      <div class="top25">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>
    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!--DICAS DENTRO-->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!--MAIS DICAS-->
<!-- ======================================================================= -->
<div class='container'>
  <div class="row">

    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">MAIS DICAS</h1>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 3");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-4 dicas_geral">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
          </a>
            <div class="caption">
              <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
        <?php
      }
    }
    ?>

  </div>
</div>
<!-- ======================================================================= -->
<!--MAIS DICAS-->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
