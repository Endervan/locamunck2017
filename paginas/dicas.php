<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_dicas.png" alt="">
        <h4>CONFIRA NOSSAS DICAS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- DICAS titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- ultimas dicas  -->
<!-- ======================================================================= -->
<div class="container top145 bottom50">
  <div class="row">
    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">ÚLTIMAS DICAS</h1>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND()");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-4 dicas_geral top20">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
          </a>
            <div class="caption">
              <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
        <?php
      }
    }
    ?>


  </div>
</div>

<!-- ======================================================================= -->
<!-- ultimas dicas  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
