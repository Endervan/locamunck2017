<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- EMPRESA titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa.png" alt="">
        <h4>CONHEÇA MAIS NOSSA EMPRESA</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- EMPRESA titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- EMPRESA DESCRICAO  -->
<!-- ======================================================================= -->
<div class="container top145">
  <div class="row">
    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">SOBRE A EMPRESA</h1>
    </div>

    <div class="col-xs-12">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>

      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- EMPRESA DESCRICAO  -->
<!-- ======================================================================= -->


<div class="container">
  <div class="row ">

    <!-- ======================================================================= -->
    <!-- MISSÃO  -->
    <!-- ======================================================================= -->
    <div class="col-xs-5">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

      <!-- ======================================================================= -->
      <!-- MISSÃO  -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- visão  -->
      <!-- ======================================================================= -->

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

      <!-- ======================================================================= -->
      <!-- visão  -->
      <!-- ======================================================================= -->


    </div>



    <div class="col-xs-7 empresa_titulo">
      <h1 class="pb15">COMO CHEGAR</h1>
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <div class=" mapa">
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="365" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>

    <div class="col-xs-12 bottom80">
      <!-- ======================================================================= -->
      <!-- valores  -->
      <!-- ======================================================================= -->
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

      <?php
      $result = $obj_site->select("tb_atuacoes","LIMIT 15");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-4 padding0  top10 lato_black">
            <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
          </div>
          <?php
        }
      }
      ?>

      </div>
      <!-- ======================================================================= -->
      <!-- valores  -->
      <!-- ======================================================================= -->

  </div>
</div>

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
