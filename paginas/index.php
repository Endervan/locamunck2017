<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <div class="col-xs-5 empresa_titulo servico_subir">
        <h1 class="pb20">NOSSOS SERVIÇOS</h1>
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
        <div class="top30 servico_desc">
          <p><?php Util::imprime($row[descricao]); ?></span></p>
        </div>

        <div class="text-center top5">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta_servicos_home.png" alt="">
        </div>

        <div class="col-xs-10 padding0">
          <a href="<?php echo Util::caminho_projeto() ?>/servicos" class="btn btn_orcamento_servico col-xs-12 top30" title="SOLICITAR UM ORÇAMENTO">
            <i class="fa fa-shopping-cart right25" aria-hidden="true"></i>SOLICITAR UM ORÇAMENTO
          </a>
        </div>
      </div>

      <div class="col-xs-7 servico_subir1">
        <?php
        $result = $obj_site->select("tb_tipos_servicos", "order by rand() limit 4");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            //  busca a qtd de produtos cadastrados
            // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


            switch ($i) {
              case 1:
              $cor_class = 'DIREITO_TRIBUTÁRIO';
              break;
              case 2:
              $cor_class = 'DIREITO_CIVIL';
              break;
              case 3:
              $cor_class = 'DIREIT_ DO_TRABALHO';
              break;
              case 4:
              $cor_class = 'DIREITO_TRIBUTÁRIO';
              break;
              case 5:
              $cor_class = 'DIREITO_CIVIL';
              break;

              default:
              $cor_class = 'DIREITO_DO_TRABALHO';
              break;
            }
            $i++;
            ?>
            <!-- ======================================================================= -->
            <!--ITEM 01 REPETI ATE 6 ITENS   -->
            <!-- ======================================================================= -->
            <div class="col-xs-6  bg_branco servicos pt15 pb15 <?php echo $cor_class; ?> ">

              <div class="media">
                <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                </a>
                <div class="media-body">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                    <h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
                    <div class=" servicos_desc">
                      <p><?php Util::imprime($row[descricao]); ?></p>
                    </div>
                  </a>
                </div>

              </div>


            </div>

            <?php
          }
        }
        ?>
      </div>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- EQUIPAMANTOS HOME -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row ">

    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">NOSSOS EQUIPAMENTOS</h1>
    </div>
    <div class="col-xs-12 ">
      <!-- ======================================================================= -->
      <!--slider EQUIPAMENTOS   -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_equipamentos.php') ?>
      <!-- ======================================================================= -->
      <!--slider EQUIPAMENTOS   -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>
</div>
</div>
<!-- ======================================================================= -->
<!-- EQUIPAMANTOS HOME -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->
<div class="container-fluid">
<div class="row top105 bg_empresa_home">
  <div class="container">
    <div class="row ">

      <div class="col-xs-6 empresa_home">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
        <h6><span>CONHEÇA A </span>LOKAMUNCK</h6>
        <div class="top15 empresa_desc">
          <p><?php Util::imprime($row[descricao]); ?></span></p>
        </div>

        <a href="<?php echo Util::caminho_projeto() ?>/empresa" class="btn btn_empresa_home top20">
          SAIBA MAIS
        </a>

        <a href="<?php Util::imprime($config[link_place]); ?>" class="btn btn_empresa_home top20 left15" target="_blank">
          SAIBA COMO CHEGAR
        </a>
      </div>

    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- nossas dicas  -->
<!-- ======================================================================= -->
<div class="container top50 bottom50">
  <div class="row">
    <div class="col-xs-12 dicas_titulo">
      <h1 class="pb20">NOSSAS DICAS</h1>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 3");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-4 dicas_geral">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption">
              <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas dicas  -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
