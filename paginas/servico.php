<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_tipos_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- servicos titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_servicos.png" alt="">
        <h4>CONHEÇA NOSSOS SERVIÇOS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- servicos titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--SERVICO DENTRO-->
<!-- ======================================================================= -->
<div class='container bottom90'>
  <div class="row top40 pb40 bg_branco">

    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20">SOBRE O SERVIÇO</h1>
    </div>

    <div class="col-xs-12 servico_Dentro">

      <div class="top25">
        <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
      </div>

      <div class="top25">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>

    </div>

    <div class="col-xs-3">
      <a href="javascript:void(0);" class="btn btn_orcamento col-xs-12 top30" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
        <i class="fa fa-shopping-cart right25" aria-hidden="true"></i>SOLICITAR UM ORÇAMENTO
      </a>
    </div>


  </div>
</div>
<!-- ======================================================================= -->
<!--SERVICO DENTRO-->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
