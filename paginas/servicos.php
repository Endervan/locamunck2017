<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- servicos titulo -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_servicos.png" alt="">
        <h4>CONHEÇA NOSSOS SERVIÇOS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- servicos titulo -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- serviços decricao  -->
<!-- ======================================================================= -->
<div class="container top50 bottom50">
  <div class="row ">
    <div class=" ">
      <?php
      $result = $obj_site->select("tb_tipos_servicos");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          //  busca a qtd de produtos cadastrados
          // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


          switch ($i) {
            case 1:
            $cor_class = 'DIREITO_TRIBUTÁRIO';
            break;
            case 2:
            $cor_class = 'DIREITO_CIVIL';
            break;
            case 3:
            $cor_class = 'DIREIT_ DO_TRABALHO';
            break;
            case 4:
            $cor_class = 'DIREITO_TRIBUTÁRIO';
            break;
            case 5:
            $cor_class = 'DIREITO_CIVIL';
            break;

            default:
            $cor_class = 'DIREITO_DO_TRABALHO';
            break;
          }
          $i++;
          ?>
          <!-- ======================================================================= -->
          <!--ITEM 01 REPETI ATE 6 ITENS   -->
          <!-- ======================================================================= -->
            <div class="col-xs-6   servicos  top30 <?php echo $cor_class; ?> ">

            <div class="media bg_branco pg20">
              <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
              </a>
              <div class="media-body">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                  <h2 class="media-heading"><?php Util::imprime($row[titulo]); ?></h2>
                  <div class="top20 servicos_desc">
                    <p><?php Util::imprime($row[descricao]); ?></p>
                  </div>
                </a>
              </div>

            </div>


          </div>

          <?php
        }
      }
      ?>


    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!--  serviços decricao  -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
